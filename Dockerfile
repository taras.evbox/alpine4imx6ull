FROM ubuntu:18.04

MAINTAINER Taras Zaporozhets <taras.zaporozhets@evbox.com>

LABEL description="Container with everything needed to build Alpine bootable image"


# Install dependencies
RUN apt-get update               \
 && apt-get -y -q upgrade        \
 && apt-get -y -q install        \
    locales                      \
    bc                           \
    binutils-arm-linux-gnueabihf \
    build-essential              \
    ccache                       \
    gcc-arm-linux-gnueabihf      \
    git                          \
    libncurses-dev               \
    libssl-dev                   \
    u-boot-tools                 \
    wget                         \
    xz-utils                     \
    flex                         \
    bison                        \
    lzop                         \
    module-init-tools            \
    autoconf                     \
    pkg-config                   \
    libconfuse-dev               \
    dosfstools                   \
    mtools                       \
    squashfs-tools               \
 && apt-get clean

# Build genimage
RUN cd /tmp/ && \
    git clone https://github.com/pengutronix/genimage.git && \
    cd genimage && \
    ./autogen.sh && \
    ./configure CFLAGS='-g -O0' --prefix=/usr && \
    make && \
    make install


# Set the locale
RUN sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && \
    locale-gen
ENV LANG en_US.UTF-8  
ENV LANGUAGE en_US:en  
ENV LC_ALL en_US.UTF-8     

# Setup user
RUN useradd -ms /bin/bash br-user && \
    chown -R br-user:br-user /home/br-user

USER br-user
WORKDIR /home/br-user
ENV HOME /home/br-user


ENV ARCH="arm"
ENV CROSS_COMPILE="arm-linux-gnueabihf-"

CMD ["bash"]
