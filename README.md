Alpine4IMX6ULL
=========================

Repo with everything needed to build Alpine bootable image for NXP IMX6ULL EVK borad.


Build a bootable image
=========================

host:

`docker build -t imx6 .`

`docker run --rm --hostname "$(hostname)" --interactive --tty -v $(pwd):/workdir/ imx6`

container:

`./build.sh`


Create a bootable SD card
=========================

Script prepares a bootable "sdcard.img" image in the images/
directory, ready to be dumped on the eMMC card. Launch the following
command as root:

  dd status=progress if=images/sdcard.img of=/dev/<your-sd-device>


*** WARNING! This will destroy all the card content. Use with care! ***

For details about the medium image layout, see the definition in genimage.cfg.


To boot your newly created system:
- put a micro USB cable into the Debug USB Port and connect using a terminal
  emulator at 115200 bps, 8n1.
- power on the board.
- loging with root.
- setup alpine with `setup-alpine` command in interactive mode, more info: 
- save midifications from ram to storage with `lbu commit`, more info here: https://wiki.alpinelinux.org/wiki/Alpine_local_backup
- now system is ready. To install additional software, please use `apk`: more info: https://wiki.alpinelinux.org/wiki/Alpine_Linux_package_management
- for cross compilation please use musl-based toolchains from:https://musl.cc/armv7l-linux-musleabihf-cross.tgz
