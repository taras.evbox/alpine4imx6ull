#!/bin/sh

set -e

TOP_DIR=$(readlink -f $(dirname $0))


make_uboot()
{
	echo "#############################################################################"
	echo "Compiling bootloader"
	echo "#############################################################################"
	
	cd ${TOP_DIR}/u-boot


	make mx6ull_14x14_evk_plugin_defconfig
	make -j4
	make u-boot.imx

	cp u-boot.imx ${TOP_DIR}/images/

	# Prepare u-boot boot sctipt
	cd ${TOP_DIR}
	mkimage -C none -A arm -T script -d boot.cmd images/boot.scr
}

make_kernel()
{
	echo "#############################################################################"
	echo "Compiling kernel"
	echo "#############################################################################"

	cd ${TOP_DIR}/linux

	make alpine_defconfig

	make -j4
	make zImage

	make modules_install INSTALL_MOD_PATH=${TOP_DIR}/images/modules
	cp arch/arm/boot/zImage ${TOP_DIR}/images/
	cp arch/arm/boot/dts/imx6ull-14x14-evk.dtb ${TOP_DIR}/images/
}

make_rootfs()
{
	echo "#############################################################################"
	echo "Making rootfs"
	echo "#############################################################################"

	cd ${TOP_DIR}
	
	mkdir -p images/alpine
	tar -xf alpine-uboot-3.11.3-armv7.tar.gz --directory images/alpine

	# Create an U-Boot image file from it initramfs
	mkimage -n initramfs-lts -A arm -O linux -T ramdisk -C none -d images/alpine/boot/initramfs-lts images/initramfs

	# Pack kernel modules to squashfs image
	mksquashfs images/modules/lib/ images/modloop -b 1048576 -comp xz -Xdict-size 100%


	cp images/alpine/alpine.apkovl.tar.gz images/
	cp -R images/alpine/apks images/
}

make_image()
{
	echo "#############################################################################"
	echo "Packing everything to SD card images"
	echo "#############################################################################"

	local GENIMAGE_CFG="genimage.cfg"
	local GENIMAGE_TMP="$(mktemp --suffix genimage.tmp)"

	local TARGET_DIR="./"
	local BINARIES_DIR="images/"

	
	rm -fd ${GENIMAGE_TMP}

	genimage \
		--rootpath "${TARGET_DIR}" \
		--tmppath "${GENIMAGE_TMP}" \
		--inputpath "${BINARIES_DIR}" \
		--outputpath "${BINARIES_DIR}" \
		--config "${GENIMAGE_CFG}"
}

main()
{
	echo "Yo!"
	
	rm -rf images || true
	mkdir -p images
	
	make_uboot
	make_kernel
	make_rootfs
	make_image

	echo "Done!"

}

main "${@}"

exit 0
