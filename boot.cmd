setenv initramfs_addr "0x84000000"

echo Booting from mmc ...
setenv bootargs "console=${console},${baudrate} modules=loop,squashfs,sd-mod,usb-storage modloop=/modloop"
run loadfdt
fatload mmc ${mmcdev}:${mmcpart} ${initramfs_addr} initramfs
run loadimage

bootz ${loadaddr} ${initramfs_addr} ${fdt_addr};

